EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C4
U 1 1 6181401E
P 5700 3150
F 0 "C4" H 5815 3196 50  0000 L CNN
F 1 "100n/3V3" H 5815 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5738 3000 50  0001 C CNN
F 3 "~" H 5700 3150 50  0001 C CNN
F 4 "C1525" H 5700 3150 50  0001 C CNN "LCSC"
	1    5700 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 61814DAF
P 7900 4450
F 0 "C5" H 8015 4496 50  0000 L CNN
F 1 "100n/3V3" H 8015 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7938 4300 50  0001 C CNN
F 3 "~" H 7900 4450 50  0001 C CNN
F 4 "C1525" H 7900 4450 50  0001 C CNN "LCSC"
	1    7900 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 618154E0
P 5250 3150
F 0 "C3" H 5365 3196 50  0000 L CNN
F 1 "100n/3V3" H 5365 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5288 3000 50  0001 C CNN
F 3 "~" H 5250 3150 50  0001 C CNN
F 4 "C1525" H 5250 3150 50  0001 C CNN "LCSC"
	1    5250 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4300 7900 4150
Wire Wire Line
	7900 4150 7800 4150
Wire Wire Line
	8000 2850 7200 2850
Wire Wire Line
	7000 3150 7000 2850
Connection ~ 7000 2850
Wire Wire Line
	7000 2850 5700 2850
Wire Wire Line
	7200 3150 7200 3000
Connection ~ 7200 2850
Wire Wire Line
	7200 2850 7000 2850
$Comp
L power:GND #PWR04
U 1 1 6181B4D9
P 7100 4850
F 0 "#PWR04" H 7100 4600 50  0001 C CNN
F 1 "GND" H 7105 4677 50  0000 C CNN
F 2 "" H 7100 4850 50  0001 C CNN
F 3 "" H 7100 4850 50  0001 C CNN
	1    7100 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4850 7100 4550
Wire Wire Line
	5250 3000 5250 2850
Connection ~ 5250 2850
Wire Wire Line
	5250 2850 4950 2850
Wire Wire Line
	5700 3000 5700 2850
Connection ~ 5700 2850
Wire Wire Line
	5700 2850 5250 2850
$Comp
L power:GND #PWR02
U 1 1 6181EA97
P 5100 3400
F 0 "#PWR02" H 5100 3150 50  0001 C CNN
F 1 "GND" H 5105 3227 50  0000 C CNN
F 2 "" H 5100 3400 50  0001 C CNN
F 3 "" H 5100 3400 50  0001 C CNN
	1    5100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3400 5100 3350
Wire Wire Line
	5100 3350 5250 3350
Wire Wire Line
	5250 3350 5250 3300
Wire Wire Line
	5700 3350 5700 3300
Wire Wire Line
	5250 3350 5700 3350
Connection ~ 5250 3350
Wire Wire Line
	4550 3150 4550 3350
Wire Wire Line
	4550 3350 4950 3350
Connection ~ 5100 3350
$Comp
L power:GND #PWR01
U 1 1 618255B6
P 2400 6250
F 0 "#PWR01" H 2400 6000 50  0001 C CNN
F 1 "GND" H 2405 6077 50  0000 C CNN
F 2 "" H 2400 6250 50  0001 C CNN
F 3 "" H 2400 6250 50  0001 C CNN
	1    2400 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6182821E
P 2200 5250
F 0 "R1" V 1993 5250 50  0000 C CNN
F 1 "100" V 2084 5250 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2130 5250 50  0001 C CNN
F 3 "~" H 2200 5250 50  0001 C CNN
F 4 "C25076" H 2200 5250 50  0001 C CNN "LCSC"
	1    2200 5250
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 61828A85
P 2400 5500
F 0 "C1" H 2515 5546 50  0000 L CNN
F 1 "4u7" H 2515 5455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2438 5350 50  0001 C CNN
F 3 "~" H 2400 5500 50  0001 C CNN
F 4 "C19666" H 2400 5500 50  0001 C CNN "LCSC"
	1    2400 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5350 2400 5250
Wire Wire Line
	2350 5250 2400 5250
Connection ~ 2400 5250
Wire Wire Line
	2400 5650 2400 5700
Wire Wire Line
	2750 5700 2400 5700
Connection ~ 2400 5700
Wire Wire Line
	2400 5700 2400 6100
Wire Wire Line
	4550 3350 3900 3350
Connection ~ 4550 3350
Wire Wire Line
	3900 3350 3900 3250
Wire Wire Line
	3900 2850 4250 2850
$Comp
L Device:C C2
U 1 1 6183DDB4
P 3900 3100
F 0 "C2" H 4015 3146 50  0000 L CNN
F 1 "10u/10V" H 4015 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3938 2950 50  0001 C CNN
F 3 "~" H 3900 3100 50  0001 C CNN
F 4 "C19702" H 3900 3100 50  0001 C CNN "LCSC"
	1    3900 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 618507C5
P 2750 4900
F 0 "JP1" H 2750 5085 50  0000 C CNN
F 1 "Jumper_NO_Small" H 2750 5000 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 2750 4900 50  0001 C CNN
F 3 "~" H 2750 4900 50  0001 C CNN
	1    2750 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 61851D2E
P 3150 4900
F 0 "R2" V 2943 4900 50  0000 C CNN
F 1 "10k" V 3034 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3080 4900 50  0001 C CNN
F 3 "~" H 3150 4900 50  0001 C CNN
F 4 "C25744" H 3150 4900 50  0001 C CNN "LCSC"
	1    3150 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 4900 2850 4900
Wire Wire Line
	3300 4900 3450 4900
$Comp
L Connector:Conn_01x06_Male J1
U 1 1 6185661A
P 4250 6450
F 0 "J1" V 4404 6062 50  0000 R CNN
F 1 "Conn_01x06_Male" V 4313 6062 50  0000 R CNN
F 2 "Connector_Molex:Molex_PicoBlade_53047-0610_1x06_P1.25mm_Vertical" H 4250 6450 50  0001 C CNN
F 3 "~" H 4250 6450 50  0001 C CNN
	1    4250 6450
	0    -1   -1   0   
$EndComp
Connection ~ 2400 6100
Wire Wire Line
	2400 6100 2400 6250
$Comp
L Device:Q_NMOS_GSD Q1
U 1 1 6188051E
P 3650 5650
F 0 "Q1" H 3854 5696 50  0000 L CNN
F 1 "2N7002" H 3854 5605 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3850 5750 50  0001 C CNN
F 3 "~" H 3650 5650 50  0001 C CNN
F 4 "C8545" H 3650 5650 50  0001 C CNN "LCSC"
	1    3650 5650
	1    0    0    -1  
$EndComp
Text Label 3750 4900 0    50   ~ 0
3V3
Wire Wire Line
	3750 4900 3750 5050
Wire Wire Line
	3750 5850 3750 6100
Wire Wire Line
	2400 6100 3750 6100
Wire Wire Line
	2750 5450 2750 5700
Wire Wire Line
	2750 5350 2750 5450
Connection ~ 2750 5450
$Comp
L agg-kicad:IRM-H6xxT IC1
U 1 1 618000BC
P 3050 5350
F 0 "IC1" H 3050 5675 50  0000 C CNN
F 1 "IRM-H6xxT" H 3050 5584 50  0000 C CNN
F 2 "agg:IRM-H6XXT" H 2850 5050 50  0001 L CNN
F 3 "https://datasheet.lcsc.com/szlcsc/2010221806_Everlight-Elec-IRM-H638T-TR2-DX_C390031.pdf" H 2850 4950 50  0001 L CNN
F 4 "C390031" H 2850 4850 50  0001 L CNN "LCSC"
	1    3050 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4900 3450 5250
Wire Wire Line
	3450 5250 3350 5250
Connection ~ 3450 5250
Wire Wire Line
	3450 5250 3450 5650
$Comp
L Device:R R3
U 1 1 6188C80E
P 3750 5200
F 0 "R3" V 3543 5200 50  0000 C CNN
F 1 "3k9" V 3634 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3680 5200 50  0001 C CNN
F 3 "~" H 3750 5200 50  0001 C CNN
F 4 "C51721" H 3750 5200 50  0001 C CNN "LCSC"
	1    3750 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 5350 3750 5400
Wire Wire Line
	3750 5400 4150 5400
Connection ~ 3750 5400
Wire Wire Line
	3750 5400 3750 5450
Text Label 4050 6200 1    50   ~ 0
5V
Wire Wire Line
	4050 6200 4050 6250
Text Label 4550 6200 1    50   ~ 0
GND
Wire Wire Line
	4550 6200 4550 6250
Text Label 3750 2850 0    50   ~ 0
5V
Wire Wire Line
	3900 2850 3900 2950
Wire Wire Line
	3900 2850 3750 2850
Connection ~ 3900 2850
Wire Wire Line
	2050 5250 2000 5250
Wire Wire Line
	2400 5250 2550 5250
Wire Wire Line
	2000 4900 2000 5250
Wire Wire Line
	2000 4900 2650 4900
Connection ~ 2000 5250
Wire Wire Line
	2000 5250 1900 5250
Text Label 1900 5250 0    50   ~ 0
5V
Wire Wire Line
	4150 5400 4150 6250
$Comp
L Device:R R4
U 1 1 618BA766
P 4450 4150
F 0 "R4" V 4243 4150 50  0000 C CNN
F 1 "3k9" V 4334 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4380 4150 50  0001 C CNN
F 3 "~" H 4450 4150 50  0001 C CNN
F 4 "C51721" H 4450 4150 50  0001 C CNN "LCSC"
	1    4450 4150
	-1   0    0    1   
$EndComp
Text Label 8000 2850 0    50   ~ 0
3V3
$Comp
L Device:R R5
U 1 1 618DA415
P 4800 4150
F 0 "R5" V 4593 4150 50  0000 C CNN
F 1 "3k9" V 4684 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4730 4150 50  0001 C CNN
F 3 "~" H 4800 4150 50  0001 C CNN
F 4 "C51721" H 4800 4150 50  0001 C CNN "LCSC"
	1    4800 4150
	-1   0    0    1   
$EndComp
$Comp
L Device:Jumper_NO_Small JP2
U 1 1 618DF07B
P 4450 4450
F 0 "JP2" H 4450 4635 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4450 4550 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 4450 4450 50  0001 C CNN
F 3 "~" H 4450 4450 50  0001 C CNN
	1    4450 4450
	0    1    1    0   
$EndComp
$Comp
L Device:Jumper_NO_Small JP3
U 1 1 618E0C20
P 4800 4450
F 0 "JP3" H 4800 4635 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4800 4550 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 4800 4450 50  0001 C CNN
F 3 "~" H 4800 4450 50  0001 C CNN
	1    4800 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 4350 4800 4300
Wire Wire Line
	4450 4300 4450 4350
Text Label 4350 6200 1    50   ~ 0
SDA
Text Label 4450 6200 1    50   ~ 0
SCL
Wire Wire Line
	4450 6200 4450 6250
Wire Wire Line
	4350 6250 4350 6200
Text Label 6350 3550 2    50   ~ 0
SDA
Text Label 6350 3650 2    50   ~ 0
SCL
Wire Wire Line
	4450 4000 4450 3950
Wire Wire Line
	4800 3950 4800 4000
Wire Wire Line
	4450 3950 4800 3950
Text Label 4450 4600 2    50   ~ 0
SDA
Wire Wire Line
	4450 4600 4450 4550
Wire Wire Line
	4800 4600 4800 4550
Text Label 4800 4600 2    50   ~ 0
SCL
Text Label 4800 3950 0    50   ~ 0
3V3
NoConn ~ 5250 5500
$Comp
L Device:C C6
U 1 1 6191EE9B
P 4950 3100
F 0 "C6" H 5065 3146 50  0000 L CNN
F 1 "22u/6V3" H 5065 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4988 2950 50  0001 C CNN
F 3 "~" H 4950 3100 50  0001 C CNN
F 4 "C59461" H 4950 3100 50  0001 C CNN "LCSC"
	1    4950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2950 4950 2850
Connection ~ 4950 2850
Wire Wire Line
	4950 2850 4850 2850
Wire Wire Line
	4950 3250 4950 3350
Connection ~ 4950 3350
Wire Wire Line
	4950 3350 5100 3350
$Comp
L power:GND #PWR0101
U 1 1 61956DE8
P 7900 4650
F 0 "#PWR0101" H 7900 4400 50  0001 C CNN
F 1 "GND" H 7905 4477 50  0000 C CNN
F 2 "" H 7900 4650 50  0001 C CNN
F 3 "" H 7900 4650 50  0001 C CNN
	1    7900 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4650 7900 4600
$Comp
L Mechanical:MountingHole H1
U 1 1 6196516D
P 2150 1650
F 0 "H1" H 2250 1696 50  0000 L CNN
F 1 "MountingHole" H 2250 1605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580" H 2150 1650 50  0001 C CNN
F 3 "~" H 2150 1650 50  0001 C CNN
	1    2150 1650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 61965BD8
P 2150 1900
F 0 "H2" H 2250 1946 50  0000 L CNN
F 1 "MountingHole" H 2250 1855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580" H 2150 1900 50  0001 C CNN
F 3 "~" H 2150 1900 50  0001 C CNN
	1    2150 1900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 6199D666
P 2150 2150
F 0 "H3" H 2250 2196 50  0000 L CNN
F 1 "MountingHole" H 2250 2105 50  0000 L CNN
F 2 "Headtrack:CableTie" H 2150 2150 50  0001 C CNN
F 3 "~" H 2150 2150 50  0001 C CNN
	1    2150 2150
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D1
U 1 1 619A213A
P 2800 3250
F 0 "D1" H 3144 3296 50  0000 L CNN
F 1 "WS2812B" H 3144 3205 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 2850 2950 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 2900 2875 50  0001 L TNN
F 4 " C2761795" H 2800 3250 50  0001 C CNN "LCSC"
	1    2800 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 619A60C2
P 2200 3100
F 0 "C7" H 2315 3146 50  0000 L CNN
F 1 "10u/10V" H 2315 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2238 2950 50  0001 C CNN
F 3 "~" H 2200 3100 50  0001 C CNN
F 4 "C19702" H 2200 3100 50  0001 C CNN "LCSC"
	1    2200 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2950 2800 2900
Wire Wire Line
	2800 2900 2200 2900
Wire Wire Line
	2200 2900 2200 2950
NoConn ~ 3100 3250
Text Label 2800 2900 0    50   ~ 0
5V
Text Label 2800 3650 0    50   ~ 0
GND
Wire Wire Line
	2800 3650 2800 3600
Wire Wire Line
	2800 3600 2200 3600
Wire Wire Line
	2200 3600 2200 3250
Connection ~ 2800 3600
Wire Wire Line
	2800 3600 2800 3550
Wire Wire Line
	2450 3250 2500 3250
Text Label 2450 3250 2    50   ~ 0
LED_DATA
Text Label 4250 6200 1    50   ~ 0
LED_DATA
Wire Wire Line
	4250 6200 4250 6250
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 61A19590
P 4050 6200
F 0 "#FLG0101" H 4050 6275 50  0001 C CNN
F 1 "PWR_FLAG" V 4050 6327 50  0000 L CNN
F 2 "" H 4050 6200 50  0001 C CNN
F 3 "~" H 4050 6200 50  0001 C CNN
	1    4050 6200
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 61A1EAA2
P 2550 5250
F 0 "#FLG0103" H 2550 5325 50  0001 C CNN
F 1 "PWR_FLAG" H 2550 5423 50  0000 C CNN
F 2 "" H 2550 5250 50  0001 C CNN
F 3 "~" H 2550 5250 50  0001 C CNN
	1    2550 5250
	1    0    0    -1  
$EndComp
Connection ~ 2550 5250
Wire Wire Line
	2550 5250 2750 5250
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 61A25252
P 4550 6200
F 0 "#FLG0102" H 4550 6275 50  0001 C CNN
F 1 "PWR_FLAG" H 4550 6373 50  0000 C CNN
F 2 "" H 4550 6200 50  0001 C CNN
F 3 "~" H 4550 6200 50  0001 C CNN
	1    4550 6200
	1    0    0    -1  
$EndComp
$Comp
L 2021-11-04_12-49-28:MMC5983MA U3
U 1 1 61A41A00
P 8350 850
F 0 "U3" H 9550 1237 60  0000 C CNN
F 1 "MMC5983MA" H 9550 1131 60  0000 C CNN
F 2 "Package_LGA:LGA-16_3x3mm_P0.5mm" H 9550 1090 60  0001 C CNN
F 3 "" H 8350 850 60  0000 C CNN
	1    8350 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3550 6400 3550
Wire Wire Line
	6350 3650 6400 3650
$Comp
L Sensor_Motion:MPU-6050 U2
U 1 1 61A35958
P 7100 3850
F 0 "U2" H 7100 3061 50  0000 C CNN
F 1 "MPU-6050" H 7100 2970 50  0000 C CNN
F 2 "Sensor_Motion:InvenSense_QFN-24_4x4mm_P0.5mm" H 7100 3050 50  0001 C CNN
F 3 "https://store.invensense.com/datasheets/invensense/MPU-6050_DataSheet_V3%204.pdf" H 7100 3700 50  0001 C CNN
	1    7100 3850
	1    0    0    -1  
$EndComp
NoConn ~ 8350 850 
Text Label 8300 950  2    50   ~ 0
SCL
Text Label 8300 1150 2    50   ~ 0
3V3
Text Label 8300 1350 2    50   ~ 0
SDA
$Comp
L Device:C C9
U 1 1 61A651DD
P 8100 1650
F 0 "C9" H 8215 1696 50  0000 L CNN
F 1 "10u/10V" H 8215 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8138 1500 50  0001 C CNN
F 3 "~" H 8100 1650 50  0001 C CNN
F 4 "C19702" H 8100 1650 50  0001 C CNN "LCSC"
	1    8100 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	8250 1650 8350 1650
Text Label 7900 1650 2    50   ~ 0
GND
Wire Wire Line
	7900 1650 7950 1650
Text Label 7900 1850 2    50   ~ 0
3V3
Wire Wire Line
	7900 1850 7950 1850
Wire Wire Line
	8300 1850 8300 1950
Wire Wire Line
	8300 1950 8350 1950
Connection ~ 8300 1850
Wire Wire Line
	8300 1850 8350 1850
Text Label 7900 2350 2    50   ~ 0
GND
Wire Wire Line
	8300 2350 8300 2250
Wire Wire Line
	8300 2250 8350 2250
Wire Wire Line
	8300 2350 8350 2350
Connection ~ 8300 2350
Wire Wire Line
	8300 950  8350 950 
Wire Wire Line
	8350 1150 8300 1150
Wire Wire Line
	8300 1350 8350 1350
NoConn ~ 10750 850 
Text Label 6300 4150 2    50   ~ 0
GND
Wire Wire Line
	6300 4150 6350 4150
$Comp
L Device:C C10
U 1 1 61A91E18
P 8550 4450
F 0 "C10" H 8665 4496 50  0000 L CNN
F 1 "2n2/3V3" H 8665 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8588 4300 50  0001 C CNN
F 3 "~" H 8550 4450 50  0001 C CNN
F 4 "C12530" H 8550 4450 50  0001 C CNN "LCSC"
	1    8550 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4050 8550 4050
Wire Wire Line
	8550 4050 8550 4300
$Comp
L power:GND #PWR0102
U 1 1 61A94E96
P 8550 4650
F 0 "#PWR0102" H 8550 4400 50  0001 C CNN
F 1 "GND" H 8555 4477 50  0000 C CNN
F 2 "" H 8550 4650 50  0001 C CNN
F 3 "" H 8550 4650 50  0001 C CNN
	1    8550 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 4650 8550 4600
$Comp
L Device:C C8
U 1 1 61A9AA65
P 7950 3200
F 0 "C8" H 8065 3246 50  0000 L CNN
F 1 "100n/3V3" H 8065 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7988 3050 50  0001 C CNN
F 3 "~" H 7950 3200 50  0001 C CNN
F 4 "C1525" H 7950 3200 50  0001 C CNN "LCSC"
	1    7950 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 3050 7950 3000
Wire Wire Line
	7950 3000 7200 3000
Connection ~ 7200 3000
Wire Wire Line
	7200 3000 7200 2850
$Comp
L power:GND #PWR0103
U 1 1 61A9DE81
P 7950 3400
F 0 "#PWR0103" H 7950 3150 50  0001 C CNN
F 1 "GND" H 7955 3227 50  0000 C CNN
F 2 "" H 7950 3400 50  0001 C CNN
F 3 "" H 7950 3400 50  0001 C CNN
	1    7950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 3400 7950 3350
NoConn ~ 7800 3550
Wire Wire Line
	6400 4050 6350 4050
Wire Wire Line
	6350 4050 6350 4150
Connection ~ 6350 4150
Wire Wire Line
	6350 4150 6400 4150
Wire Wire Line
	6400 3750 6350 3750
Wire Wire Line
	6350 3750 6350 4050
Connection ~ 6350 4050
NoConn ~ 7800 3750
NoConn ~ 7800 3850
$Comp
L Regulator_Linear:XC6206PxxxMR U1
U 1 1 61AB93B8
P 4550 2850
F 0 "U1" H 4550 3092 50  0000 C CNN
F 1 "XC6206PxxxMR" H 4550 3001 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4550 3075 50  0001 C CIN
F 3 "https://www.torexsemi.com/file/xc6206/XC6206.pdf" H 4550 2850 50  0001 C CNN
	1    4550 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 61B033C3
P 7950 2100
F 0 "C11" H 8065 2146 50  0000 L CNN
F 1 "2n2/3V3" H 8065 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7988 1950 50  0001 C CNN
F 3 "~" H 7950 2100 50  0001 C CNN
F 4 "C12530" H 7950 2100 50  0001 C CNN "LCSC"
	1    7950 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1950 7950 1850
Connection ~ 7950 1850
Wire Wire Line
	7950 1850 8300 1850
Wire Wire Line
	7950 2250 7950 2350
Wire Wire Line
	7950 2350 8300 2350
Wire Wire Line
	7900 2350 7950 2350
Connection ~ 7950 2350
$EndSCHEMATC
